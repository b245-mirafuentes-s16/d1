// console.log("hello World");

// [Section] Arithmetic Operators
	// allow us to perform mathematical operatios between operands (value on either sides of the operators)
	// it returns a numerical value.

	let x = 102;
	let y = 25;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	let quotient = x / y;
	console.log("Result of division operator: " + quotient);


	let remainder = x % y;
	console.log("Result of modulo operator: " + remainder);
	// Assignment Operator (=)
		// assigns the value of the "right operand" to a variable.
	// Basic Assignment Operator
	let assignmentNumber = 8;
	console.log(assignmentNumber);

	// Addition Assignment Operator (+=)
	// long method
	// assignmentNumber = assignmentNumber + 2;

	// shorthand method
	assignmentNumber += 2;
	console.log("Results of addition assignment operator: " + assignmentNumber);

	// Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)

	assignmentNumber -= 2;
	console.log("Results of subtraction assignment operator: " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Results of multiplication assignment operator: " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Results of division assignment operator: " + assignmentNumber);

	assignmentNumber %= 2;
	console.log("Results of modulo assignment operator: " + assignmentNumber);

	// PEMDAS (order of operations)

	// Multiple Operators and Parenthesis
	/*
		1. 3*4 = 12
		2. 12/5 = 2.4
		3. 1+2 = 3
		4. 3-2.4 = 0.6

	*/
	let mdas = 1 +2 - 3 * 4 / 5;
	console.log("Results of MDAS operator: " +mdas);

	// The order of operations can be changed by adding a parenthesis

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Results of PEMDAS operation: " +pemdas);

	pemdas = (1 + (2 - 3)) * (4 / 5);
	console.log("Results of PEMDAS operation: " +pemdas);

// [Section] Increment and Decrement
	// This operators add or subtract values by 1 and reassign the value of the variables where the increment/decrement was applied

	let z = 1;

	// Increment (++)
	// the value of "z" is added by one before storing it in the increment variable
	let increment = ++z;
	// pre-increment both increment and z variable have a value of "2"
	console.log("Results of pre-increment: " +increment);
	console.log("Results of pre-increment for z: " +z);

	// The value of "z" is stored in the "increment" variable before it is increased by one
	increment = z++;
	console.log("Results of post-increment: " + increment);
	console.log("Results of post-increment for z: " + z);

	// Decrement (--)
	let decrement = --z;
	console.log("Results of pre-decrement: " + decrement);
	console.log("Results of pre-decrement for z: " + z);

	decrement = z--;
	console.log("Results of post-decrement: " + decrement);
	console.log("Results of post-decrement for z: " + z);

	// concept for increment/decrement
	// pre - z + z = z
	// post - z = z + z

// [Section] Type Coercion
	// automatic or implicit concersion of value from one data type to another
	// This happens when operations are performed on different data types that would normally not be possible and yield irregular results.
	// "automatic conversion"

	let	numA = '10';
	let numB = 12;

	// Performing addition operation to a number and string variable will result to concatenation.
	let coercion = numB + numA;
	console.log(coercion)
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;

	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	/*
		- The boolean true is also associated with the value of 1
		- The boolean false is also associated with the value of 0

	*/

	let numE = true + 1;
	console.log(numE);

	let numF = false + 1;
	console.log(numF);

// [Section] Comparison Operator
	// are used to evaluate and compare the left and right operands
	// it returns a boolean value
	
	let juan = 'juan';

	// Equality Operator (==)
	/*
		- Checks whether the operands are equal/have the same content.
		- Attempts to CONVERT AND COMPARE operands of different data types (type coercion)
	*/

	console.log("Equality Operator: ");
	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == '1'); // true
	console.log(false == 0); // true
	console.log('juan' == "juan"); //true
	console.log("juan" == "Juan"); //false
	console.log(juan == 'juan'); //true

	// Inequality Operator (!=)
	/*
		- Checks whether the operands are not equal/have different data types
		- Attemps to CONVERT AND COMPARE operands of different data types
	*/
	console.log("Ineqaulity Operator: ");
	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != '1'); // false
	console.log(false != 0); // false
	console.log('juan' != "juan"); //false
	console.log("juan" != "Juan"); //true
	console.log(juan != 'juan'); //false

	// Strict Equality Operator (===)
	/*
		- Check whether the operands are equal/have the same content.
		- also COMPARES the data type of 2 values.
	*/

	console.log("Strict Equality Operator: ");
	console.log(1 === 1); //true
	console.log(1 === 2); //false
	console.log(1 === '1'); //false
	console.log(false === 0); //false
	console.log('juan' === "juan"); //true
	console.log("juan" === "Juan"); //false
	console.log(juan === 'juan'); //true

	// Strict Inequality Operator (!==)
	/*
		- Checks whether the operands are not equal/have different content
		- also compares the data type of 2 values
	*/
	console.log("Strict Inequality Operator: ");
	console.log(1 !== 1); //false
	console.log(1 !== 2); //true
	console.log(1 !== '1'); //true
	console.log(false !== 0); //true
	console.log('juan' !== "juan"); //false
	console.log("juan" !== "Juan"); //true
	console.log(juan !== 'juan'); //false

	// [Section] Greater Than and Less Than Operator
		// Some comparison operated check whether one value is greater or less than to the other value
		// Returns a bolean value

	let a = 50;
	let b = 65;

	console.log("Greater Than or Less Than Operator: ")
	// GT or greater than (>)
	let isGreaterThan = a > b;
	console.log(isGreaterThan);

	// LT or less than (<)

	let isLessThan = a < b;
	console.log(isLessThan);

	// GTE or Greater Than or Equal (>=)
	// b = 50; // changing the value of b to 50 will result to true
	let isGTorEqual = a >= b;
	console.log(isGTorEqual);

	// LTE or Less Than or Equal (<=)
	a = 66;
	let isLTorEqual = a <= b;
	console.log(isLTorEqual);

	let numStr = "30";
	console.log(a > numStr); //true - forced coercion to change string to a number

	let strNum = "twenty"
	console.log(b > strNum); // whatever operator is used it will result to false, since the string is not numeric. This is usually result to NaN (Not a Number)

// [Section] Logical Operator
	//  to allow us to compare more specific logical combination of conditions and evaluations.
	// It returns a boolean value.

	let isLegalAge = true;
	let isRegistered = false;

	// Logical AND Operator (&& - Double Ampersand)
	// Returns TRUE if all operands are TRUE.
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical AND operator: " + allRequirementsMet);

	// Logical OR Operator (|| - Double Pipe)
	// Returns TRUE if one of the operands are TRUE
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR operator: " + someRequirementsMet);

	// Logical NOT Operator (! - Exclamation Point)
	// Returns the opposite value.
	console.log("Result of logical NOT Operator: " + !isRegistered);
	console.log("Result of logical NOT Operator: " + !isLegalAge);